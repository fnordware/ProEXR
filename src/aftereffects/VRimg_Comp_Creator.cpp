
/* ---------------------------------------------------------------------
// 
// ProEXR - OpenEXR plug-ins for Photoshop and After Effects
// Copyright (c) 2007-2017,  Brendan Bolles, http://www.fnordware.com
// 
// This file is part of ProEXR.
//
// ProEXR is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 2 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
// 
// -------------------------------------------------------------------*/

#include "VRimg_Comp_Creator.h"

#include "OpenEXR_PlatformIO.h"

#include "VRimgVersion.h"
#include "VRimgInputFile.h"

#include "ProEXR_UTF.h"

#include <Iex.h>

#include <vector>
#include <list>


using namespace std;
using namespace Imath;



extern AEGP_PluginID S_mem_id;
extern AEGP_Command gCompCreatorCmd;

static SPBasicSuite			*sP							=	NULL;

AEGP_InstalledEffectKey		gEXtractoR_key				=	NULL;
AEGP_InstalledEffectKey		gIDentifier_key				=	NULL;


enum {
	DO_EXTRACT = 1,
	DO_COPY,
	DO_FULL_ON,
	DO_FULL_OFF,
	DO_NOTHING
};
typedef A_char	ExtractAction;

#define MAX_CHANNEL_NAME_LEN 127

typedef struct {
	ExtractAction	action;
	A_long			index; // 0-based index in the file
	char			reserved[27]; // total of 32 bytes up to here
	A_char			name[MAX_CHANNEL_NAME_LEN+1];
} ChannelData;


typedef struct {
	A_u_char		version; // version of this data structure
	A_Boolean		single_channel;
	char			reserved[30]; // total of 32 bytes up to here
	ChannelData		red;
	ChannelData		green;
	ChannelData		blue;
	ChannelData		alpha;
} EXtractoRArbitraryData;

#define EXTRACTOR_ARB_INDEX		1
#define EXTRACTOR_UNMULT_INDEX	5


typedef struct {
	A_u_char		version; // version of this data structure
	A_long			index; // 0-based index in the file
	char			reserved[11]; // total of 16 bytes up to here
	A_char			name[MAX_CHANNEL_NAME_LEN+1];
} IDentifierArbitraryData;

#define IDENTIFIER_ARB_INDEX	1


#ifndef MAX_LAYER_NAME_LEN
#define MAX_LAYER_NAME_LEN 63 // same as PF_CHANNEL_NAME_LEN
#endif


#ifdef AE_HFS_PATHS
// convert from HFS paths (fnord:Users:mrb:) to Unix paths (/Users/mrb/)
static int ConvertPath(const char * inPath, char * outPath, int outPathMaxLen)
{
	CFStringRef inStr = CFStringCreateWithCString(kCFAllocatorDefault, inPath ,kCFStringEncodingMacRoman);
	if (inStr == NULL)
		return -1;
	CFURLRef url = CFURLCreateWithFileSystemPath(kCFAllocatorDefault, inStr, kCFURLHFSPathStyle,0);
	CFStringRef outStr = CFURLCopyFileSystemPath(url, kCFURLPOSIXPathStyle);
	if (!CFStringGetCString(outStr, outPath, outPathMaxLen, kCFURLPOSIXPathStyle))
		return -1;
	CFRelease(outStr);
	CFRelease(url);
	CFRelease(inStr);
	return 0;
}
#endif // AE_HFS_PATHS

#pragma mark-

// like a thicker string, but not quite a rope, get it?
template <typename T>
class yarn
{
  public:
	yarn();
	yarn(const yarn &other);
	yarn(const T *ptr);
	yarn(const string &s); 
	~yarn();
	
	yarn & operator = (const yarn &other);
	
	const T *get() const { return _p; }
	operator const T * () const { return get(); }
	
	string str() const;
	operator string () const { return str(); }
	
	size_t size() const;
	
  private:
	T *_p;
};


template <typename T>
yarn<T>::yarn()
{
	_p = new T[1];
	_p[0] = '\0';
}


template <typename T>
yarn<T>::yarn(const yarn &other)
{
	size_t len = other.size() + 1;

	_p = new T[len];
	
	memcpy(_p, other.get(), len * sizeof(T));
}


template <typename T>
yarn<T>::yarn(const T *ptr)
{
	const T *p = ptr;
	size_t len = 0;
	
	while(*p++ != '\0')
		len++;
		
	len++; // make space for the null

	_p = new T[len];
	
	memcpy(_p, ptr, len * sizeof(T));
}


template <>
yarn<char>::yarn(const string &s)
{
	size_t len = s.size() + 1;

	_p = new char[len];
	
	strncpy(_p, s.c_str(), len);
}


template <>
yarn<utf16_char>::yarn(const string &s)
{
	size_t len = s.size() + 1;

	_p = new utf16_char[len];
	
	UTF8toUTF16(s, _p, len);
}


template <typename T>
yarn<T> &
yarn<T>::operator = (const yarn &other)
{
	if(this != &other) // prevent self-assignment
	{
		delete [] _p;
		
		size_t len = other.size() + 1;

		_p = new T[len];
		
		memcpy(_p, other.get(), len * sizeof(T));
	}
	
	return *this;
}


template <typename T>
yarn<T>::~yarn()
{
	delete [] _p;
}


template<>
string
yarn<char>::str() const
{
	return string(_p);
}


template<>
string
yarn<utf16_char>::str() const
{
	return UTF16toUTF8(_p);
}


template <typename T>
size_t
yarn<T>::size() const
{
	T *p = _p;
	size_t len = 0;
	
	while(*p++ != '\0')
		len++;
	
	return len;
}

typedef yarn<A_NameType> AeName;


#pragma mark-


static void ResizeName(string &str, unsigned int len)
{
	// only applies to pre-unicode names
#ifndef AE_UNICODE_NAMES
	if(str.size() > len)
		str.resize(len);
#endif
}


static AeName GetName(AEGP_SuiteHandler &suites, AEGP_ItemH itemH)
{
	AeName name;
	
#ifdef AE_UNICODE_NAMES
	AEGP_MemHandle nameH = NULL;
	
	suites.ItemSuite()->AEGP_GetItemName(S_mem_id, itemH, &nameH);
	
	if(nameH)
	{
		AEGP_MemSize size = 0;
		suites.MemorySuite()->AEGP_GetMemHandleSize(nameH, &size);
		
		A_NameType *nameP = NULL;
		suites.MemorySuite()->AEGP_LockMemHandle(nameH, (void **)&nameP);
		
		name = nameP;
		
		suites.MemorySuite()->AEGP_FreeMemHandle(nameH);
	}
#else
	A_NameType nameP[AEGP_MAX_ITEM_NAME_SIZE];
	
	suites.ItemSuite()->AEGP_GetItemName(itemH, nameP);
	
	name = nameP;
#endif

	return name;
}


static string GetBaseName(AEGP_SuiteHandler &suites, AEGP_ItemH itemH)
{
	string base_name = GetName(suites, itemH).str();
	
	
	size_t last_dot = base_name.find_last_of('.');
	
	if(last_dot != string::npos && last_dot > 0 && last_dot < base_name.size() - 1)
	{
		const string extension = base_name.substr(last_dot + 1);
		
		if(extension == "exr" || extension == "vri" || extension == "vrimg")
			base_name = base_name.substr(0, last_dot);
	}
	
	
	last_dot = base_name.find_last_of('.');
	
	if(last_dot != string::npos && last_dot > 0 && last_dot < base_name.size() - 2)
	{
		if(base_name[last_dot + 1] == '[')
			base_name = base_name.substr(0, last_dot);
	}

	
	return base_name;
}


static A_Boolean ItemIsVRimg(AEGP_ItemH itemH, A_Boolean require_single)
{
	AEGP_SuiteHandler	suites(sP);
	
	A_Boolean isVRimg = FALSE;
	
	AEGP_ItemType type;
	suites.ItemSuite()->AEGP_GetItemType(itemH, &type);
	
	if(type == AEGP_ItemType_FOOTAGE)
	{
		AEGP_ItemFlags flagsH;
		
		suites.ItemSuite()->AEGP_GetItemFlags(itemH, &flagsH);
		
		if( !(flagsH & AEGP_ItemFlag_MISSING) )
		{
			AEGP_FootageH footH;
			suites.FootageSuite()->AEGP_GetMainFootageFromItem(itemH, &footH);
			
			if(require_single)
			{
				A_long num_files, files_per_frame;
				
				suites.FootageSuite()->AEGP_GetFootageNumFiles(footH, &num_files, &files_per_frame);
				
				if(num_files > 1)
					return FALSE;
			}
			
			
			PathString path;
			string char_path;
			
		#ifdef AE_UNICODE_PATHS	
			AEGP_MemHandle u_pathH = NULL;
			A_PathType *file_pathZ = NULL;
		
			suites.FootageSuite()->AEGP_GetFootagePath(footH, 0, AEGP_FOOTAGE_MAIN_FILE_INDEX, &u_pathH);
			
			if(u_pathH)
			{
				suites.MemorySuite()->AEGP_LockMemHandle(u_pathH, (void **)&file_pathZ);
				
				path = file_pathZ;
				char_path = UTF16toUTF8(file_pathZ);
				
				suites.MemorySuite()->AEGP_FreeMemHandle(u_pathH);
			}
		#else
			A_char pathZ[AEGP_MAX_PATH_SIZE+1];
			suites.FootageSuite()->AEGP_GetFootagePath(footH, 0, AEGP_FOOTAGE_MAIN_FILE_INDEX, pathZ);

		#ifdef AE_HFS_PATHS
			if(pathZ[0] != '\0')
				ConvertPath(pathZ, pathZ, AEGP_MAX_PATH_SIZE);
		#endif
		
			path = pathZ;
			char_path = pathZ;
		#endif
			
			if(char_path.length() > 0)
			{
				string the_extension = char_path.substr( char_path.size() - 3, 3 );
				
				if(the_extension == "vri" || the_extension == "VRI" ||
					the_extension == "img" || the_extension == "IMG")
				{
					try{
					
					// got this off the OpenEXR site
					IStreamPlatform f(path.string());

					char bytes[4];
					f.read(bytes, sizeof(bytes));

					// here's the check
					if( VRimg::isVRimgMagic(bytes) )
					{
						isVRimg = TRUE;
					}
					
					}catch(...) {}
				}
			}
		}
	}
	
	return isVRimg;
}


static A_Err NewCompFromFootageItem(AEGP_ItemH itemH, AEGP_LayerH *layerH)
{
	A_Err			err 		= A_Err_NONE;
	
	AEGP_SuiteHandler	suites(sP);
	

	AEGP_ItemType type;
	AEGP_ItemFlags flagsH;

	suites.ItemSuite()->AEGP_GetItemType(itemH, &type);
	suites.ItemSuite()->AEGP_GetItemFlags(itemH, &flagsH);

	if(type == AEGP_ItemType_FOOTAGE && !(flagsH & AEGP_ItemFlag_MISSING) )
	{
		// parent folder
		AEGP_ItemH parent_folderH;
		suites.ItemSuite()->AEGP_GetItemParentFolder(itemH, &parent_folderH);
		
		// put the item in a new folder
		AeName name = GetBaseName(suites, itemH);
		
		AEGP_ItemH exr_folderH;
		suites.ItemSuite()->AEGP_CreateNewFolder(name, parent_folderH, &exr_folderH );
		
		// move footage in
		suites.ItemSuite()->AEGP_SetItemParentFolder(itemH, exr_folderH);
		
		
		// everything you need to know about the footage
		A_long width, height;
		A_Ratio pixel_aspect;
		A_Time duration;
		AEGP_FootageInterp interp;
		A_Ratio framerate;
		suites.ItemSuite()->AEGP_GetItemDimensions(itemH, &width, &height);
		suites.ItemSuite()->AEGP_GetItemPixelAspectRatio(itemH, &pixel_aspect);
		suites.ItemSuite()->AEGP_GetItemDuration(itemH, &duration);
		suites.FootageSuite()->AEGP_GetFootageInterpretation(itemH, FALSE, &interp);
		
		if(interp.native_fpsF < 0.1 || duration.value == 0)
		{
			// get the sequence frame rate from preferences
			AEGP_PersistentBlobH blobH = NULL;
			A_FpLong framerate = 24.0;
			
			suites.PersistentDataSuite()->AEGP_GetApplicationBlob(&blobH);
			suites.PersistentDataSuite()->AEGP_GetFpLong(blobH, "General Section", "Default Import Sequence FPS", 24.0, &framerate);

			interp.native_fpsF = framerate;
		}
			
		framerate.den = 1000; framerate.num = (1000.0 * interp.native_fpsF) + 0.5;
		
		if(duration.value == 0)
			duration.value = 10 * duration.scale;
		
		// assemble comp
		string master_comp_name = name;
		
		ResizeName(master_comp_name, (AEGP_MAX_ITEM_NAME_SIZE-10));
		
		master_comp_name += " assemble";
		
		AeName master_comp_nameAE = master_comp_name;

		AEGP_CompH master_compH;
		suites.CompSuite()->AEGP_CreateComp(exr_folderH, master_comp_nameAE, width, height,
												&pixel_aspect, &duration, &framerate, &master_compH);

		// add footage to source comp
		suites.LayerSuite()->AEGP_AddLayer(itemH, master_compH, layerH);
	}
	
	return err;
}


static A_Err BuildVRimgCompsFromLayer(AEGP_LayerH layerH)
{
	A_Err			err 		= A_Err_NONE;
	
	AEGP_SuiteHandler	suites(sP);

	try{
	

	AEGP_ItemH layer_itemH = NULL;
	suites.LayerSuite()->AEGP_GetLayerSourceItem(layerH, &layer_itemH);
	
	if( layer_itemH && ItemIsVRimg(layer_itemH, FALSE) )
	{
		// basic stuff we'll be needing
		AEGP_CompH master_compH;
		AEGP_ItemH master_comp_itemH;
		suites.LayerSuite()->AEGP_GetLayerParentComp(layerH, &master_compH);
		suites.CompSuite()->AEGP_GetItemFromComp(master_compH, &master_comp_itemH); 

		AEGP_ItemH parent_folderH;
		suites.ItemSuite()->AEGP_GetItemParentFolder(master_comp_itemH, &parent_folderH);
		
		
		// we need the path so we can read the file, duh
		AEGP_FootageH footH;
		suites.FootageSuite()->AEGP_GetMainFootageFromItem(layer_itemH, &footH);
		
		// get interpretation for Alpha purposes
		AEGP_FootageInterp interp;
		suites.FootageSuite()->AEGP_GetFootageInterpretation(layer_itemH, FALSE, &interp);
		
		
		A_char path[AEGP_MAX_PATH_SIZE+1];
	
	#ifdef AE_UNICODE_PATHS	
		AEGP_MemHandle u_pathH = NULL;
		A_PathType *file_pathZ = NULL;
		suites.FootageSuite()->AEGP_GetFootagePath(footH, 0, AEGP_FOOTAGE_MAIN_FILE_INDEX, &u_pathH);
		
		if(u_pathH)
		{
			suites.MemorySuite()->AEGP_LockMemHandle(u_pathH, (void **)&file_pathZ);
			
			string char_path = UTF16toUTF8(file_pathZ);
			
			strncpy(path, char_path.c_str(), AEGP_MAX_PATH_SIZE);
		}
	#else
		suites.FootageSuite()->AEGP_GetFootagePath(footH, 0, AEGP_FOOTAGE_MAIN_FILE_INDEX, path);
		
		A_PathType *file_pathZ = path;
	#endif
		
	#ifdef AE_HFS_PATHS
		ConvertPath(path, path, AEGP_MAX_PATH_SIZE);
	#endif

		
		const string filename = GetBaseName(suites, layer_itemH);
		

		// get info from the VRimg
		IStreamPlatform in_stream(file_pathZ);
		
		VRimg::InputFile input(in_stream);
		
	#ifdef AE_UNICODE_PATHS	
		if(u_pathH)
			suites.MemorySuite()->AEGP_FreeMemHandle(u_pathH);
	#endif
	
		// create a folder to hold source comps
		AEGP_ItemH comps_folderH;
		
		string comps_folder_name = filename;
		if(comps_folder_name.size() > (AEGP_MAX_ITEM_NAME_SIZE-14) )
			comps_folder_name.resize(AEGP_MAX_ITEM_NAME_SIZE-14);
			
		comps_folder_name += " source comps";
		
		AeName comps_folder_nameAE = comps_folder_name;
		
		suites.ItemSuite()->AEGP_CreateNewFolder(comps_folder_nameAE, parent_folderH, &comps_folderH );
		
		
		// everything you need to know about the master comp
		A_long width, height;
		A_Ratio pixel_aspect;
		A_Time duration;
		A_FpLong framerate;
		A_Ratio framerateRatio;
		
		suites.ItemSuite()->AEGP_GetItemDimensions(master_comp_itemH, &width, &height);
		suites.ItemSuite()->AEGP_GetItemPixelAspectRatio(master_comp_itemH, &pixel_aspect);
		suites.ItemSuite()->AEGP_GetItemDuration(master_comp_itemH, &duration);
		suites.CompSuite()->AEGP_GetCompFramerate(master_compH, &framerate);
		
		framerateRatio.den = 1000; framerateRatio.num = (framerateRatio.den * framerate) + 0.5;
		
		
		// everything you need to know about the source layer
		AEGP_StreamRefH stream_refH; // this is used later on down
		AEGP_StreamRefH anchor_stream, position_stream, rotation_stream, scale_stream;
		AEGP_StreamValue2 anchor_value, position_value, rotation_value, scale_value;
		AEGP_MemHandle anchor_expH, position_expH, rotation_expH, scale_expH; // expression handles
		A_Boolean anchor_exp_enabled, position_exp_enabled, rotaiton_exp_enabled, scale_exp_enabled;
		A_Time time_begin = {0, 1};
		
		
	#define GET_LAYER_STREAM_VALUE(LAYER, PARAM, STREAM, VALP, EXPH, EXPBOOL) \
		suites.StreamSuite()->AEGP_GetNewLayerStream(S_mem_id, layerH, PARAM, &STREAM); \
		suites.StreamSuite()->AEGP_GetNewStreamValue(S_mem_id, STREAM, AEGP_LTimeMode_LayerTime, &time_begin, TRUE, VALP); \
		EXPH = NULL; EXPBOOL = TRUE; \
		suites.StreamSuite()->AEGP_GetExpression(S_mem_id, STREAM, &EXPH); \
		if(EXPH){ suites.StreamSuite()->AEGP_GetExpressionState(S_mem_id, STREAM, &EXPBOOL); }

		GET_LAYER_STREAM_VALUE(layerH, AEGP_LayerStream_ANCHORPOINT, anchor_stream, &anchor_value, anchor_expH, anchor_exp_enabled)
		GET_LAYER_STREAM_VALUE(layerH, AEGP_LayerStream_POSITION, position_stream, &position_value, position_expH, position_exp_enabled);
		GET_LAYER_STREAM_VALUE(layerH, AEGP_LayerStream_ROTATION, rotation_stream, &rotation_value, rotation_expH, rotaiton_exp_enabled);
		GET_LAYER_STREAM_VALUE(layerH, AEGP_LayerStream_SCALE, scale_stream, &scale_value, scale_expH, scale_exp_enabled);
		
		
		// build a list of layer names
		vector<string> layer_list;
		
		bool have_rgb = false;
		bool have_alpha = false;
		
		const VRimg::Header::LayerMap &layers = input.header().layers();
		
		for(VRimg::Header::LayerMap::const_iterator i = layers.begin(); i != layers.end(); i++)
		{
			string channel_name = i->first;
			
			if(channel_name == "RGB color")
				have_rgb = true;
			else if(channel_name == "Alpha")
				have_alpha = true;
			else
				layer_list.push_back(channel_name);
		}
		
		// making sure these appear at the top
		if(have_alpha)
			layer_list.push_back("Alpha");
		
		if(have_rgb)
			layer_list.push_back("RGB color");
		

		if(layer_list.size() < 1)
			throw Iex::LogicExc("No layers found.");
		
		

		// are we building a Contact sheet?
	#define PREFS_SECTION		"ProEXR"
	#define CONTACT_SHEET_SIZE	"Contact Sheet Size"
		AEGP_PersistentBlobH blobH = NULL;
		suites.PersistentDataSuite()->AEGP_GetApplicationBlob(&blobH);
		
		A_long contact_sheet_mult = 2;
		suites.PersistentDataSuite()->AEGP_GetLong(blobH, PREFS_SECTION, CONTACT_SHEET_SIZE, contact_sheet_mult, &contact_sheet_mult);
		
		AEGP_CompH contact_sheet_compH = NULL;
		int contact_tiles_x = 1, contact_tiles_y = 1;
		
		if(contact_sheet_mult > 0 && layer_list.size() > 1)
		{
			const int total_tiles = layer_list.size();
		
			while(contact_tiles_x * contact_tiles_y < total_tiles)
			{
				if(contact_tiles_y > contact_tiles_x)
					contact_tiles_x++;
				else
					contact_tiles_y++;
			}
			
			if(contact_tiles_x < 2)
				contact_sheet_mult = 1;
			
			const int contact_width = width * contact_sheet_mult;
			const int contact_height = height * contact_sheet_mult * contact_tiles_y / contact_tiles_x;
				
			string contact_sheet_name = filename;

			ResizeName(contact_sheet_name, (AEGP_MAX_ITEM_NAME_SIZE-14));
				
			contact_sheet_name += " contact sheet";
			
			AeName comps_folder_nameAE = contact_sheet_name;
		
			suites.CompSuite()->AEGP_CreateComp(parent_folderH, comps_folder_nameAE, contact_width, contact_height,
													&pixel_aspect, &duration, &framerateRatio, &contact_sheet_compH);
		}
		
		
		// create source comps, add to master comp
		for(int i=0; i < layer_list.size(); i++)
		{
			string layer_name = layer_list[i];
			
			const VRimg::Layer *layer = input.header().findLayer( layer_name );
			
			if(layer == NULL)
				throw Iex::LogicExc("Layer is NULL.");

			// source comp
			AEGP_CompH source_compH;
			
			string source_comp_name = layer_name + " source";
			
			ResizeName(source_comp_name, (AEGP_MAX_ITEM_NAME_SIZE-1));
			
			AeName source_comp_nameAE = source_comp_name;

			suites.CompSuite()->AEGP_CreateComp(comps_folderH, source_comp_nameAE, width, height,
													&pixel_aspect, &duration, &framerateRatio, &source_compH);
			
			// source comp item
			AEGP_ItemH source_comp_itemH;
			suites.CompSuite()->AEGP_GetItemFromComp(source_compH, &source_comp_itemH);
			
			// add footage to source comp
			AEGP_LayerH footage_layerH;
			suites.LayerSuite()->AEGP_AddLayer(layer_itemH, source_compH, &footage_layerH);
			
			
			// copy settings from original layer
	#define SET_LAYER_STREAM_VAL(LAYER, PARAM, VALP, EXPH, EXPBOOL) \
			suites.StreamSuite()->AEGP_GetNewLayerStream(S_mem_id, LAYER, PARAM, &stream_refH); \
			suites.StreamSuite()->AEGP_SetStreamValue(S_mem_id, stream_refH, VALP); \
			if(EXPH) \
			{ \
				A_char *exp = NULL; \
				suites.MemorySuite()->AEGP_LockMemHandle(EXPH, (void**)&exp); \
				suites.StreamSuite()->AEGP_SetExpression(S_mem_id, stream_refH, exp); \
				suites.StreamSuite()->AEGP_SetExpressionState(S_mem_id, stream_refH, EXPBOOL); \
			} \
			suites.StreamSuite()->AEGP_DisposeStream(stream_refH);

			SET_LAYER_STREAM_VAL(footage_layerH, AEGP_LayerStream_ANCHORPOINT, &anchor_value, anchor_expH, anchor_exp_enabled);
			SET_LAYER_STREAM_VAL(footage_layerH, AEGP_LayerStream_POSITION, &position_value, position_expH, position_exp_enabled);
			SET_LAYER_STREAM_VAL(footage_layerH, AEGP_LayerStream_ROTATION, &rotation_value, rotation_expH, rotaiton_exp_enabled);
			SET_LAYER_STREAM_VAL(footage_layerH, AEGP_LayerStream_SCALE, &scale_value, scale_expH, scale_exp_enabled);
			

			// apply EXtractoR or IDentifier (unless the layer is just "RGB" or "Y[RY][BY]")
			if(layer_name != "RGB color")
			{
				// add effect to layer
				if(layer->type == VRimg::INT && gIDentifier_key)
				{
					// set up the parameter data
					IDentifierArbitraryData arb;
					
					arb.version = 1;
					arb.index = 0;
					strncpy(arb.name, layer_name.c_str(), MAX_CHANNEL_NAME_LEN);
					
					// copy to a handle
					A_Handle handle = (A_Handle)suites.HandleSuite()->host_new_handle( sizeof(IDentifierArbitraryData) );
					void *ptr = suites.HandleSuite()->host_lock_handle((PF_Handle)handle);
					
					memcpy(ptr, &arb, sizeof(IDentifierArbitraryData) );
					
					suites.HandleSuite()->host_unlock_handle((PF_Handle)handle);
					
					
					// apply effect, assign handle
					AEGP_EffectRefH new_effectH;
					AEGP_StreamRefH arb_stream;
					AEGP_StreamValue2 val;
					
					val.val.arbH = handle;
					
					suites.EffectSuite()->AEGP_ApplyEffect(S_mem_id, footage_layerH, gIDentifier_key, &new_effectH);
					
					
					suites.StreamSuite()->AEGP_GetNewEffectStreamByIndex(S_mem_id,
										new_effectH, IDENTIFIER_ARB_INDEX, &arb_stream);
					
					suites.StreamSuite()->AEGP_SetStreamValue(S_mem_id, arb_stream, &val);
					
					//suites.StreamSuite()->AEGP_DisposeStreamValue(&val);
					
					suites.StreamSuite()->AEGP_DisposeStream(arb_stream);
					
					suites.EffectSuite()->AEGP_DisposeEffect(new_effectH);
				}
				else if(layer->type != VRimg::INT && gEXtractoR_key)
				{
					// set up the parameter data
					EXtractoRArbitraryData arb;
					ChannelData *c_ptr[] = { &arb.red, &arb.green, &arb.blue, &arb.alpha };
					
					arb.version = 1;
					arb.red.action = arb.green.action = arb.blue.action = arb.alpha.action = DO_COPY;
					arb.red.index = arb.green.index = arb.blue.index = arb.alpha.index = 0;
					
					int c = 0;
						
					if( layer->dimensions == 1 )
					{
						string channel = layer_name;
						
						strncpy(c_ptr[0]->name, channel.c_str(), MAX_CHANNEL_NAME_LEN);	c_ptr[0]->action = DO_EXTRACT;
						strncpy(c_ptr[1]->name, channel.c_str(), MAX_CHANNEL_NAME_LEN);	c_ptr[1]->action = DO_EXTRACT;
						strncpy(c_ptr[2]->name, channel.c_str(), MAX_CHANNEL_NAME_LEN);	c_ptr[2]->action = DO_EXTRACT;
					}
					else
					{
						string chan_names[4] = { layer_name + ".R", layer_name + ".G", layer_name + ".B", layer_name + ".A" };
						
						// get the non-alphas
						while(c < layer->dimensions && c < 4)
						{
							strncpy(c_ptr[c]->name, chan_names[c].c_str(), MAX_CHANNEL_NAME_LEN);
							
							c_ptr[c]->action = DO_EXTRACT;
							
							c++;
						}
					}
					
					
					// copy to a handle
					A_Handle handle = (A_Handle)suites.HandleSuite()->host_new_handle( sizeof(EXtractoRArbitraryData) );
					void *ptr = suites.HandleSuite()->host_lock_handle((PF_Handle)handle);
					
					memcpy(ptr, &arb, sizeof(EXtractoRArbitraryData) );
					
					suites.HandleSuite()->host_unlock_handle((PF_Handle)handle);
					
					
					// apply effect, assign handle
					AEGP_EffectRefH new_effectH;
					AEGP_StreamRefH arb_stream;
					AEGP_StreamValue2 val;
					
					val.val.arbH = handle;
					
					suites.EffectSuite()->AEGP_ApplyEffect(S_mem_id, footage_layerH, gEXtractoR_key, &new_effectH);
					
					
					suites.StreamSuite()->AEGP_GetNewEffectStreamByIndex(S_mem_id,
										new_effectH, EXTRACTOR_ARB_INDEX, &arb_stream);
					
					suites.StreamSuite()->AEGP_SetStreamValue(S_mem_id, arb_stream, &val);
					
					
					// UnMult if footage is interpreted as Premultipled
					if(interp.al.flags == AEGP_AlphaPremul)
					{
						AEGP_StreamRefH unmult_stream;
						AEGP_StreamValue2 u_val;
						
						u_val.val.one_d = 1.0;

						suites.StreamSuite()->AEGP_GetNewEffectStreamByIndex(S_mem_id,
											new_effectH, EXTRACTOR_UNMULT_INDEX, &unmult_stream);
						
						suites.StreamSuite()->AEGP_SetStreamValue(S_mem_id, unmult_stream, &u_val);
					
						suites.StreamSuite()->AEGP_DisposeStream(unmult_stream);
					}
					
					suites.StreamSuite()->AEGP_DisposeStream(arb_stream);
					
					suites.EffectSuite()->AEGP_DisposeEffect(new_effectH);
				}
			}

								
			// add source comp to master comp
			if( layer_name != "Alpha" || (interp.al.flags & AEGP_AlphaIgnore)) // not alpha unless not reading it as part of the footage
			{
				AEGP_LayerH master_comp_layerH;
				suites.LayerSuite()->AEGP_AddLayer(source_comp_itemH, master_compH, &master_comp_layerH);
				
				
				// set various layer parameters (if applicable)
				if(layer_name.size() > (AEGP_MAX_LAYER_NAME_SIZE-1))
					layer_name.resize(AEGP_MAX_LAYER_NAME_SIZE-1);
				
				AeName layer_nameAE = layer_name;

				suites.LayerSuite()->AEGP_SetLayerName(master_comp_layerH, layer_nameAE);
			}
			
			
			// add source comp to contact sheet
			if(contact_sheet_mult > 0 && contact_sheet_compH != NULL)
			{
				const int total_tiles = layer_list.size();
			
				const int tile_pos = total_tiles - 1 - i;
				
				const int tile_y = tile_pos / contact_tiles_x;
				const int tile_x = tile_pos % contact_tiles_x;
				
				
				AEGP_LayerH contact_comp_layerH;
				suites.LayerSuite()->AEGP_AddLayer(source_comp_itemH, contact_sheet_compH, &contact_comp_layerH);
				
				
				AEGP_StreamRefH position_stream, scale_stream;
				AEGP_StreamValue2 position_value, scale_value;
				const A_Time time_begin = {0, 1};
				
				suites.StreamSuite()->AEGP_GetNewLayerStream(S_mem_id, contact_comp_layerH, AEGP_LayerStream_POSITION, &position_stream);
				suites.StreamSuite()->AEGP_GetNewLayerStream(S_mem_id, contact_comp_layerH, AEGP_LayerStream_SCALE, &scale_stream);
				
				suites.StreamSuite()->AEGP_GetNewStreamValue(S_mem_id, position_stream, AEGP_LTimeMode_LayerTime, &time_begin, TRUE, &position_value);
				suites.StreamSuite()->AEGP_GetNewStreamValue(S_mem_id, scale_stream, AEGP_LTimeMode_LayerTime, &time_begin, TRUE, &scale_value);
				
				const float tile_width = (float)contact_sheet_mult * (float)width / (float)contact_tiles_x;
				const float tile_height = (float)contact_sheet_mult * (float)height / (float)contact_tiles_x;
				
				position_value.val.two_d.x = ((float)tile_x * tile_width) + (tile_width / 2.f);
				position_value.val.two_d.y = ((float)tile_y * tile_height) + (tile_height / 2.f);
				
				scale_value.val.two_d.x = scale_value.val.two_d.y = 100.f * contact_sheet_mult / (float)contact_tiles_x;
				
				suites.StreamSuite()->AEGP_SetStreamValue(S_mem_id, position_stream, &position_value);
				suites.StreamSuite()->AEGP_SetStreamValue(S_mem_id, scale_stream, &scale_value);
				
				suites.StreamSuite()->AEGP_DisposeStream(position_stream);
				suites.StreamSuite()->AEGP_DisposeStream(scale_stream);
			}
		}
		
		
		// dispose these stream values and streams from the original layer
	#define DISPOSE_STREAM(STREAM, VALUE, EXPH) \
		if(EXPH) { suites.MemorySuite()->AEGP_FreeMemHandle(EXPH); } \
		suites.StreamSuite()->AEGP_DisposeStreamValue(&VALUE); \
		suites.StreamSuite()->AEGP_DisposeStream(STREAM);

		DISPOSE_STREAM(anchor_stream, anchor_value, anchor_expH);
		DISPOSE_STREAM(position_stream, position_value, position_expH);
		DISPOSE_STREAM(rotation_stream, rotation_value, rotation_expH);
		DISPOSE_STREAM(scale_stream, scale_value, scale_expH);
		
		
		// now delete the original layer itself
		suites.LayerSuite()->AEGP_DeleteLayer(layerH);
	}
	
	}catch(...) {}
	
	return err;
}


A_Err
GPCommandHook(
	AEGP_GlobalRefcon	plugin_refconPV,
	AEGP_CommandRefcon	refconPV,
	AEGP_Command		command,
	AEGP_HookPriority	hook_priority,
	A_Boolean			already_handledB,
	A_Boolean			*handledPB)
{
	A_Err			err 		= A_Err_NONE;
	AEGP_SuiteHandler	suites(sP);
	
	if(command == gCompCreatorCmd)
	{
		try{

		// fix the auto cache threshold for the poor user, seeing as the Plabt refuses to fix their sequence settings bug
	#define PREFS_AUTO_CACHE "Auto Cache Threshold"
		
		AEGP_PersistentBlobH blobH = NULL;
		suites.PersistentDataSuite()->AEGP_GetApplicationBlob(&blobH);
		
		A_long auto_cache_channels = 5;
		suites.PersistentDataSuite()->AEGP_GetLong(blobH, PREFS_SECTION, PREFS_AUTO_CACHE, auto_cache_channels, &auto_cache_channels);
		
		if(auto_cache_channels == 0)
		{
			suites.PersistentDataSuite()->AEGP_SetLong(blobH, PREFS_SECTION, PREFS_AUTO_CACHE, 5);
		}
		
		
		AEGP_ProjectH projH;
		suites.ProjSuite()->AEGP_GetProjectByIndex(0, &projH);
		
		// undo
		suites.UtilitySuite()->AEGP_StartUndoGroup("VRimg Layer Comps");
		
		A_Boolean found = FALSE;
		
		// look for a selected EXR layer in a comp
		if(!found)
		{
			AEGP_ItemH itemH = NULL;
			suites.ItemSuite()->AEGP_GetActiveItem(&itemH);
			
			if(itemH)
			{
				AEGP_ItemType type;
				suites.ItemSuite()->AEGP_GetItemType(itemH, &type);
				
				if(type == AEGP_ItemType_COMP)
				{
					AEGP_CompH compH;
					AEGP_Collection2H collH = NULL;
					
					suites.CompSuite()->AEGP_GetCompFromItem(itemH, &compH);
					suites.CompSuite()->AEGP_GetNewCollectionFromCompSelection(S_mem_id, compH, &collH);
					
					if(collH)
					{
						A_u_long num_items;
						
						suites.CollectionSuite()->AEGP_GetCollectionNumItems(collH, &num_items);
						
						for(int i=0; i < num_items; i++)
						{
							AEGP_CollectionItemV2 coll_item;
							suites.CollectionSuite()->AEGP_GetCollectionItemByIndex(collH, i, &coll_item);
							
							if(coll_item.type == AEGP_CollectionItemType_LAYER)
							{
								AEGP_ItemH layer_itemH;
								suites.LayerSuite()->AEGP_GetLayerSourceItem(coll_item.u.layer.layerH, &layer_itemH);
								
								if(layer_itemH)
								{
									if( ItemIsVRimg(layer_itemH, FALSE) )
									{
										BuildVRimgCompsFromLayer(coll_item.u.layer.layerH);
										
										found = TRUE;
									}
								}
							}
						}
					
						suites.CollectionSuite()->AEGP_DisposeCollection(collH);
					}
				}
			}


			if(!found)
			{
				AEGP_ItemH itemH;
				suites.ItemSuite()->AEGP_GetFirstProjItem(projH, &itemH);
				
				list<AEGP_ItemH> vrimg_list;
				
				// look for selected VRimg files
				do{
					A_Boolean is_selected;
					suites.ItemSuite()->AEGP_IsItemSelected(itemH, &is_selected);
				
					if(itemH && is_selected)
					{
						if( ItemIsVRimg(itemH, FALSE) )
						{
							vrimg_list.push_back(itemH);
							
							found = TRUE;
						}
					}
				}while( !suites.ItemSuite()->AEGP_GetNextProjItem(projH, itemH, &itemH) && itemH );
				
				// got VRimg items (without messing with project ordering yet), now iterate
				for(list<AEGP_ItemH>::const_iterator i = vrimg_list.begin(); i != vrimg_list.end(); i++)
				{
					AEGP_LayerH layerH = NULL;
					
					// make a new comp with this footage...
					NewCompFromFootageItem(*i, &layerH);
					
					// ...then replace the footage with source comps, etc.
					if(layerH != NULL)
						BuildVRimgCompsFromLayer(layerH);
				}
			}
		}
		
		
		// undo
		suites.UtilitySuite()->AEGP_EndUndoGroup();

		}catch(...) {}
		
		// I handled it, right?
		*handledPB = TRUE;
	}
		
	return err;

}




A_Err
UpdateMenuHook(
	AEGP_GlobalRefcon		plugin_refconPV,
	AEGP_UpdateMenuRefcon	refconPV,
	AEGP_WindowType			active_window)
{
	A_Err 				err 			=	A_Err_NONE;		
	AEGP_SuiteHandler	suites(sP);

	try{
	
	A_Boolean found = FALSE;
	
	suites.CommandSuite()->AEGP_SetMenuCommandName(gCompCreatorCmd, COMP_CREATOR_MENU_STR);
	
	
	// look for a selected VRimg layer in a comp
	if(!found)
	{
		AEGP_ItemH itemH = NULL;
		suites.ItemSuite()->AEGP_GetActiveItem(&itemH);
		
		if(itemH)
		{
			AEGP_ItemType type;
			suites.ItemSuite()->AEGP_GetItemType(itemH, &type);
			
			if(type == AEGP_ItemType_COMP)
			{
				AEGP_CompH compH;
				AEGP_Collection2H collH = NULL;
				
				suites.CompSuite()->AEGP_GetCompFromItem(itemH, &compH);
				suites.CompSuite()->AEGP_GetNewCollectionFromCompSelection(S_mem_id, compH, &collH);
				
				if(collH)
				{
					A_u_long num_items;
					
					suites.CollectionSuite()->AEGP_GetCollectionNumItems(collH, &num_items);
					
					for(int i=0; i < num_items && !found; i++)
					{
						AEGP_CollectionItemV2 coll_item;
						
						suites.CollectionSuite()->AEGP_GetCollectionItemByIndex(collH, i, &coll_item);
						
						if(coll_item.type == AEGP_CollectionItemType_LAYER)
						{
							AEGP_ItemH layer_itemH;
							
							suites.LayerSuite()->AEGP_GetLayerSourceItem(coll_item.u.layer.layerH, &layer_itemH);
							
							if(layer_itemH && ItemIsVRimg(layer_itemH, FALSE))
								found = TRUE;
						}
					}
				}
				
				if(collH)
					suites.CollectionSuite()->AEGP_DisposeCollection(collH);
			}
		}
	}
	
	
	// check project for selected VRimg footage items
	if(!found)
	{
		AEGP_ProjectH projH;
		AEGP_ItemH itemH;
		
		suites.ProjSuite()->AEGP_GetProjectByIndex(0, &projH);
		suites.ItemSuite()->AEGP_GetFirstProjItem(projH, &itemH);
		
		do{
			if(itemH)
			{
				A_Boolean is_selected;
				suites.ItemSuite()->AEGP_IsItemSelected(itemH, &is_selected);
				
				if(is_selected && ItemIsVRimg(itemH, FALSE))
					found = TRUE;
			}

		}while( !suites.ItemSuite()->AEGP_GetNextProjItem(projH, itemH, &itemH) && itemH && !found);
	}


	if(found)
		suites.CommandSuite()->AEGP_EnableCommand(gCompCreatorCmd);
	

	}catch(...) {}
		
	return err;
}


void GetControlKeys(SPBasicSuite *pica_basicP)
{
	// also do other inits here
	sP = pica_basicP;
	

	AEGP_InstalledEffectKey next_key, prev_key = AEGP_InstalledEffectKey_NONE;
	A_char	plug_name[PF_MAX_EFFECT_NAME_LEN + 1];

	AEGP_SuiteHandler suites(sP);
	
#define EXTRACTOR_KEY "EXtractoR"
#define IDENTIFIER_KEY "IDentifier"

	suites.EffectSuite()->AEGP_GetNextInstalledEffect(prev_key, &next_key);
	
	while( (!gEXtractoR_key || !gIDentifier_key) && next_key)
	{
		suites.EffectSuite()->AEGP_GetEffectMatchName(next_key, plug_name);
		
		if(string(plug_name) == string(EXTRACTOR_KEY) )
			gEXtractoR_key = next_key;
		else if(string(plug_name) == string(IDENTIFIER_KEY) )
			gIDentifier_key = next_key;
				
		prev_key = next_key;

		suites.EffectSuite()->AEGP_GetNextInstalledEffect(prev_key, &next_key);
	}
}

