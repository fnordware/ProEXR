/* ---------------------------------------------------------------------
// 
// ProEXR - OpenEXR plug-ins for Photoshop and After Effects
// Copyright (c) 2007-2017,  Brendan Bolles, http://www.fnordware.com
// 
// This file is part of ProEXR.
//
// ProEXR is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 2 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
// 
// -------------------------------------------------------------------*/

#ifndef __ProEXR_Version_H__
#define __ProEXR_Version_H__

#define ProEXR_Major_Version 2
#define ProEXR_Minor_Version 6
#define ProEXR_Version_String "2.6"
#define ProEXR_Build_Date __DATE__
#define ProEXR_Build_Date_Manual "6 May 2020"
#define ProEXR_Build_Complete_Manual "v2.6 - 6 May 2020"
#define ProEXR_Copyright_Year "2007-2020"
#define ProEXR_Build_Year "2019"

#define ProEXR_Category "OpenEXR"
#define ProEXR_Priority 2
#define ProEXR_EZ_Priority 1

#define ProEXR_Description "A file format module for using OpenEXR files in Adobe Photoshop®."

#endif // __ProEXR_Version_H__
